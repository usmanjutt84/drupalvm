<?php
/**
 * @file
 * paddle_ckeditor_profiles.features.inc
 */

/**
 * Implements hook_views_api().
 */
function paddle_ckeditor_profiles_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
