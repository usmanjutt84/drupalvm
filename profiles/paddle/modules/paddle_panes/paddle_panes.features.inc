<?php
/**
 * @file
 * paddle_panes.features.inc
 */

/**
 * Implements hook_views_api().
 */
function paddle_panes_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function paddle_panes_image_default_styles() {
  $styles = array();

  // Exported image style: top_section_icon.
  $styles['top_section_icon'] = array(
    'label' => 'Top section icon',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 45,
          'height' => 45,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
