var themeTableHeaderOffset = function() {
  var offsetheight = jQuery("#header").height();
  return offsetheight;
};
;
/**
 * @file
 * Javascript functionality to fold/unfold panes.
 */
(function ($) {
  Drupal.behaviors.foldable = {
    attach: function (context, settings) {
      // Add a span to be used as a hook for the icon only once.
      // @see http://archive.plugins.jquery.com/project/once
      $(".foldable h2.pane-title").once(function () {
        $("<span>").addClass( "foldable-ui-icon").prependTo($(this));
      });
      // Unfold the pane/tab that has an error in it.
      if ($("input.error").length > 0) {
        $("input.error").each(function () {
          $(this).parents('.foldable').removeClass('folded').end().parents('.pane-content').addClass('pane-visible');
        });
      }

      // Hide the content of all folded panes.
      $('div.foldable.folded').children('div.pane-content').addClass('pane-hidden').attr('aria-hidden', true);

      // Fold/unfold panes when clicked.
      $('.foldable h2.pane-title').click(function () {
        var content_panes = $(this).siblings('div.pane-content');
        var content_pane = content_panes[0];
        if ($(this).parent().hasClass('folded')) {
          $(content_pane).slideDown({
            duration: 'fast',
            easing: 'linear',
            complete: function () {
              $(content_pane).removeClass('pane-hidden').addClass('pane-visible');
              $(content_pane).attr('aria-hidden', false);
              $(this).parent().removeClass('folded');
            }
          });
        }
        else {
          $(content_pane).slideUp({
            duration: 'fast',
            complete: function () {
              $(content_pane).removeClass('pane-visible').addClass('pane-hidden');
              $(content_pane).attr('aria-hidden', true);
              $(this).parent().addClass('folded');
            }
          });
        }
      });
    }
  }
})(jQuery);
;
/**
 * @file
 *
 * Implements a styled browse button.
 *
 */
 (function ($) {
  Drupal.behaviors.browseButton = {
    attach: function (context, settings) {
      // Check for file inputs.
      if($('input:file').length == 0) {
        return;
      }
      // Fix clickable area for IE.
      if ($.browser.msie) {
        $('.input-file-wrapper').bind('mousemove',function(e){
        var offset = $(this).offset();
        $(this).find('input:file').css({
            'top': e.pageY - offset.top - ($('.input-file-wrapper input:file').innerHeight() / 2),
            'left': e.pageX - offset.left - ($('.input-file-wrapper input:file').innerWidth() * 0.95)
          });
        $(this).find('input:file').css('opacity',0);
        })
      }

      var inputFiles = $('.input-file-wrapper input:file');
      // Sets the filename and truncate if too long
      $.each(inputFiles, function() {
        $(this).parent().find('input[type="text"]').remove();
        if ($.browser.msie  && parseInt($.browser.version, 10) === 8) {
          $('<input type="text" class="dummy-input-text" value="' + Drupal.t("Select a file ...") + '" readonly />').insertBefore($(this));
        }
        else {
        $('<input type="text" class="dummy-input-text" placeholder="' + Drupal.t("Select a file ...") + '" readonly />').insertBefore($(this));
        }
        $(this).change(function () {
          var file = $(this).val().split(/[\\/]/);
          var imageName = file[file.length - 1];
          var truncLength = 25;
          var fileNameLength = file[file.length - 1].length;

          if (fileNameLength > truncLength) {
            var truncatedName = "..." + imageName.substring((fileNameLength - truncLength));
          }
          else {
            var truncatedName = imageName;
          }
           if ($.browser.msie  && parseInt($.browser.version, 10) === 8) {
             $(this).parent().find('input.dummy-input-text').attr('value', truncatedName);
          }
          else {
            $(this).parent().find('input.dummy-input-text').attr('placeholder', truncatedName);
          }
        });
      });
    }
  }
})(jQuery);
;
/*
 * @file
 *
 * jQuery UI Tooltip @VERSION
 *
 * Copyright (c) 2011 Marcus Ekwall (http://writeless.se/projects/
 jquery-ui-tooltip).
 * Copyright (c) 2011 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://writeless.se/projects/jquery-ui-tooltip
 * http://docs.jquery.com/UI/Tooltip
 *
 * Depends:
 *    jquery.ui.core.js
 *    jquery.ui.widget.js
 *    jquery.ui.position.js
 */
(function($) {

// Role=application on body required for screenreaders to correctly interpret
// aria attributes.
if(!$(document.body).is('[role]')){
  $(document.body).attr('role','application');
}
var increments = 0;

$.widget("ui.tooltip", {
  options: {
    tooltipClass: "ui-widget-content",
    content: function() {
      return $(this).attr("title");
    },
    duration: 0,
    position: "right",
    offset: 10,
    show: {
      effect: 'fade',
      options: {},
      speed: 500,
      callback: function(){}
    },
    hide: {
      effect: 'fade',
      options: {},
      speed: 500,
      callback: function(){}
    },
    sticky: false,
    trackMouse: false
  },
  trackMouse: false,
  _init: function() {
    var self = this;
    // Check and parse inline options.
    $.each(this.element[0].attributes, function(i,a){
      if (a.name == "tooltip") {
        var inlineOptions = a.value.split(',');
        $.each(inlineOptions, function(i,o){
          var option = o.split(':'),
            key = option[0].trim();
            value = option[1].trim();
          if (value == "true" || value == "false") {
            value = (value === "true");
          } else if (value.match(/\n+/)) {
            value = parseFloat(value);
          }
          self.options[key] = value;
        });
      }
    });

    this.tooltip = $("<div></div>")
      .attr("id", "ui-tooltip-" + increments++)
      .attr("role", "tooltip")
      .attr("aria-hidden", "true")
      .addClass("ui-tooltip ui-widget ui-corner-all")
      .addClass(this.options.tooltipClass)
      .appendTo(document.body)
      .hide();
    this.tooltipContainer = $("<div></div>")
      .addClass("ui-tooltip-container")
      .appendTo(this.tooltip);
    this.tooltipContent = $("<div></div>")
      .addClass("ui-tooltip-content")
      .appendTo(this.tooltipContainer);
    this.opacity = this.tooltip.css("opacity");

    if (this.options.sticky) {
      this.element
        .bind("click.tooltip", function(event) {
          self.open(event);
        });

      this.tooltipClose = $("<a/>")
        .attr("href", "#")
        .attr("role", "button")
        .addClass("ui-tooltip-close ui-corner-all")
        .hover(function(){
          $(this).addClass("ui-state-hover");
        }, function(){
          $(this).removeClass("ui-state-hover");
        })
        .click(function(event){
          self.close(event);
          return false;
        })
        .append(
          $("<span></span>")
            .addClass("ui-icon ui-icon-closethick")
            .text("close")
        ).prependTo(this.tooltipContainer);
    } else {
      this.element
        .bind("focus.tooltip mouseenter.tooltip", function(event) {
          self.open(event);
        })
        .bind("blur.tooltip mouseleave.tooltip", function(event) {
          var e = event;
          if (self.options.duration > 0) {
            if (this.timeout) {
              clearTimeout(self.timeout);
            }
            self.timeout = setTimeout(function(){
                self.close(e);
                self.timeout = null;
              }, self.options.duration);
          } else {
            self.close(event);
          }
        });
    }
  },

  enable: function() {
    this.options.disabled = false;
  },

  disable: function() {
    this.options.disabled = true;
  },

  destroy: function() {
    this.tooltip.remove();
    $.Widget.prototype.destroy.apply(this, arguments);
  },

  widget: function() {
    return this.tooltip;
  },

  open: function(event) {
    var target = this.element;
    // Already visible? possible when both focus and mouseover events occur.
    if (this.current && this.current[0] == target[0]) {
      return;
  }
    var self = this;
    this.current = target;
    this.currentTitle = target.attr("title");

    if (typeof this.options.content == "function") {
      var content = this.options.content.call(target[0], function(response) {
        // Ignore async responses that come in after the tooltip is already
        // hidden.
        if (self.current == target) {
          self._show(event, target, response);
        }
      });
    } else {
      var content = this.options.content;
    }
    if (content) {
      self._show(event, target, content);
    }
  },

  _show: function(event, target, content) {
    if (!content) {
      return;
    }
    if (this.options.disabled) {
      return;
    }
    if(target.attr("title")) {
      target.attr("title", "");
    }

    this.tooltipContent.html(content);

    // Adjust the z-index so we place new tooltips on top of the other.
    var zIndex = this.tooltip.css("z-index") + $(".ui-tooltip").length;
    this.tooltip.css({
      top: 0,
      left: 0,
      zIndex: zIndex
    });

    // Beware ugly positioning hack. has to be shown,
    // positioned and hidden again for animated showing or
    // else it will be positioned wrong.
    switch (this.options.position) {
      case "right":
        this.tooltip.show().position({
          my: "left center",
          at: "right center",
          of: target,
          collision: "none",
          offset: this.options.offset + " 0"
        }).hide();
      break;

      case "left":
        this.tooltip.show().position({
          my: "right center",
          at: "left center",
          of: target,
          collision: "none",
          offset: -(this.options.offset) + " 0"
        }).hide();
      break;

      case "top":
        this.tooltip.show().position({
          my: "center bottom",
          at: "center top",
          of: target,
          collision: "none",
          offset: "0 " + (-this.options.offset)
        }).hide();
      break;

      case "bottom":
        this.tooltip.show().position({
          my: "center top",
          at: "center bottom",
          of: target,
          collision: "none",
          offset: "0 " + this.options.offset
        }).hide();
      break;

    }
    target.attr("aria-describedby", this.tooltip.attr("id"));
    this.tooltip.attr("aria-hidden", "false");

    var self = this;

    // Setup position and events to track mouse.
    if (event.type != "focus" && this.options.trackMouse) {
      this.trackMouse = function(event){
        self.tooltip.position({
          my: "left center",
          at: "right center",
          of: event,
          offset: "10 10",
          collision: "fit"
        });
      };
      $(document).mousemove(this.trackMouse).mousemove();
    };

    // Bind keydown event to document to listen for esc key.
    this.escPress = function(event) {
      if (event.which == 27) {
        self.close(event);
      }
    };
    $(document).bind("keydown", this.escPress);

    if (this.tooltip.is(":animated")) {
      this.tooltip.stop().show().fadeTo("normal", this.opacity);
    } else {
      if (!this.tooltip.is(":visible")) {
        this.tooltip.show(this.options.show.effect, this.options.show.options, this.options.show.speed, this.options.show.callback);
      }
    }

    this._trigger("open", event);
  },

  close: function(event) {
    if (!this.current || this.options.disable) {
      return;
    }
    // Restore title attribute to element
    var current = this.current.attr("title", this.currentTitle);
    this.current = null;

    // Unbind mousemove event.
    if (this.options.trackMouse) {
      $(document).unbind('mousemove', this.trackMouse);
    }

    // Unbind keydown event.
    if (this.options.escPress) {
      $(document).unbind("keydown", this.escPress);
    }

    // Remove aria attributes
    current.removeAttr("aria-describedby");
    this.tooltip.attr("aria-hidden", "true");

    if (this.tooltip.is(":animated")) {
      this.tooltip.stop().fadeTo("normal", 0, function() {
        $(this).hide().css("opacity", "");
      });
    } else {
      if (this.tooltip.is(":visible")) {
        this.tooltip.hide(this.options.hide.effect, this.options.hide.options, this.options.hide.speed, this.options.hide.callback);
      }
    }
    this._trigger("close", event);
  }
});
})(jQuery);
;
/**
 * @file
 *
 * Implements tooltip for Paddle Style form elements with hidden labels.
 *
 */
 (function ($) {
  Drupal.behaviors.paddleStylePluginTooltips = {
    attach: function (context, settings) {
    $(".paddle-style-plugin label, .form-type-jquery-colorpicker").tooltip({
      show: false,
      hide: false,
      // If content returns nothing,  no tooltip will show.
      content: function () {
        // Jquery color picker.
        if($(this).hasClass('form-type-jquery-colorpicker')) {
          return $(this).find("label").text();
        }
        // Patterns and Textures.
        else if ($(this).attr('for').match(/background-pattern/) && $(this).attr('for').match(/background-pattern/) !== null && !$(this).attr('for').match(/background-pattern-upload-image/)) {
          var patternclass = $(this).attr('for');
          var labeltext = $(this).find("span.label-hidden").text();
          return '<div class="labeltext">' + labeltext + '</div><div class="' + patternclass + '"></div>';
        }
        // Other.
        else {
          return $(this).find("span.label-hidden").text();
        }
      },
      position: "bottom",
      offset: 3
    });
    }
  }
})(jQuery);
;
/*
 * ScrollToFixed
 * https://github.com/bigspotteddog/ScrollToFixed
 *
 * Copyright (c) 2011 Joseph Cava-Lynch
 * MIT license
 */
(function(a){a.isScrollToFixed=function(b){return !!a(b).data("ScrollToFixed")};a.ScrollToFixed=function(d,i){var l=this;l.$el=a(d);l.el=d;l.$el.data("ScrollToFixed",l);var c=false;var G=l.$el;var H;var E;var e;var y;var D=0;var q=0;var j=-1;var f=-1;var t=null;var z;var g;function u(){G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed");f=-1;D=G.offset().top;q=G.offset().left;if(l.options.offsets){q+=(G.offset().left-G.position().left)}if(j==-1){j=q}H=G.css("position");c=true;if(l.options.bottom!=-1){G.trigger("preFixed.ScrollToFixed");w();G.trigger("fixed.ScrollToFixed")}}function n(){var I=l.options.limit;if(!I){return 0}if(typeof(I)==="function"){return I.apply(G)}return I}function p(){return H==="fixed"}function x(){return H==="absolute"}function h(){return !(p()||x())}function w(){if(!p()){t.css({display:G.css("display"),width:G.outerWidth(true),height:G.outerHeight(true),"float":G.css("float")});cssOptions={"z-index":l.options.zIndex,position:"fixed",top:l.options.bottom==-1?s():"",bottom:l.options.bottom==-1?"":l.options.bottom,"margin-left":"0px"};if(!l.options.dontSetWidth){cssOptions.width=G.width()}G.css(cssOptions);G.addClass(l.options.baseClassName);if(l.options.className){G.addClass(l.options.className)}H="fixed"}}function b(){var J=n();var I=q;if(l.options.removeOffsets){I="";J=J-D}cssOptions={position:"absolute",top:J,left:I,"margin-left":"0px",bottom:""};if(!l.options.dontSetWidth){cssOptions.width=G.width()}G.css(cssOptions);H="absolute"}function k(){if(!h()){f=-1;t.css("display","none");G.css({"z-index":y,width:"",position:E,left:"",top:e,"margin-left":""});G.removeClass("scroll-to-fixed-fixed");if(l.options.className){G.removeClass(l.options.className)}H=null}}function v(I){if(I!=f){G.css("left",q-I);f=I}}function s(){var I=l.options.marginTop;if(!I){return 0}if(typeof(I)==="function"){return I.apply(G)}return I}function A(){if(!a.isScrollToFixed(G)){return}var K=c;if(!c){u()}else{if(h()){D=G.offset().top;q=G.offset().left}}var I=a(window).scrollLeft();var L=a(window).scrollTop();var J=n();if(l.options.minWidth&&a(window).width()<l.options.minWidth){if(!h()||!K){o();G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed")}}else{if(l.options.maxWidth&&a(window).width()>l.options.maxWidth){if(!h()||!K){o();G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed")}}else{if(l.options.bottom==-1){if(J>0&&L>=J-s()){if(!x()||!K){o();G.trigger("preAbsolute.ScrollToFixed");b();G.trigger("unfixed.ScrollToFixed")}}else{if(L>=D-s()){if(!p()||!K){o();G.trigger("preFixed.ScrollToFixed");w();f=-1;G.trigger("fixed.ScrollToFixed")}v(I)}else{if(!h()||!K){o();G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed")}}}}else{if(J>0){if(L+a(window).height()-G.outerHeight(true)>=J-(s()||-m())){if(p()){o();G.trigger("preUnfixed.ScrollToFixed");if(E==="absolute"){b()}else{k()}G.trigger("unfixed.ScrollToFixed")}}else{if(!p()){o();G.trigger("preFixed.ScrollToFixed");w()}v(I);G.trigger("fixed.ScrollToFixed")}}else{v(I)}}}}}function m(){if(!l.options.bottom){return 0}return l.options.bottom}function o(){var I=G.css("position");if(I=="absolute"){G.trigger("postAbsolute.ScrollToFixed")}else{if(I=="fixed"){G.trigger("postFixed.ScrollToFixed")}else{G.trigger("postUnfixed.ScrollToFixed")}}}var C=function(I){if(G.is(":visible")){c=false;A()}};var F=function(I){(!!window.requestAnimationFrame)?requestAnimationFrame(A):A()};var B=function(){var J=document.body;if(document.createElement&&J&&J.appendChild&&J.removeChild){var L=document.createElement("div");if(!L.getBoundingClientRect){return null}L.innerHTML="x";L.style.cssText="position:fixed;top:100px;";J.appendChild(L);var M=J.style.height,N=J.scrollTop;J.style.height="3000px";J.scrollTop=500;var I=L.getBoundingClientRect().top;J.style.height=M;var K=(I===100);J.removeChild(L);J.scrollTop=N;return K}return null};var r=function(I){I=I||window.event;if(I.preventDefault){I.preventDefault()}I.returnValue=false};l.init=function(){l.options=a.extend({},a.ScrollToFixed.defaultOptions,i);y=G.css("z-index");l.$el.css("z-index",l.options.zIndex);t=a("<div />");H=G.css("position");E=G.css("position");e=G.css("top");if(h()){l.$el.after(t)}a(window).bind("resize.ScrollToFixed",C);a(window).bind("scroll.ScrollToFixed",F);if("ontouchmove" in window){a(window).bind("touchmove.ScrollToFixed",A)}if(l.options.preFixed){G.bind("preFixed.ScrollToFixed",l.options.preFixed)}if(l.options.postFixed){G.bind("postFixed.ScrollToFixed",l.options.postFixed)}if(l.options.preUnfixed){G.bind("preUnfixed.ScrollToFixed",l.options.preUnfixed)}if(l.options.postUnfixed){G.bind("postUnfixed.ScrollToFixed",l.options.postUnfixed)}if(l.options.preAbsolute){G.bind("preAbsolute.ScrollToFixed",l.options.preAbsolute)}if(l.options.postAbsolute){G.bind("postAbsolute.ScrollToFixed",l.options.postAbsolute)}if(l.options.fixed){G.bind("fixed.ScrollToFixed",l.options.fixed)}if(l.options.unfixed){G.bind("unfixed.ScrollToFixed",l.options.unfixed)}if(l.options.spacerClass){t.addClass(l.options.spacerClass)}G.bind("resize.ScrollToFixed",function(){t.height(G.height())});G.bind("scroll.ScrollToFixed",function(){G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed");A()});G.bind("detach.ScrollToFixed",function(I){r(I);G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed");a(window).unbind("resize.ScrollToFixed",C);a(window).unbind("scroll.ScrollToFixed",F);G.unbind(".ScrollToFixed");t.remove();l.$el.removeData("ScrollToFixed")});C()};l.init()};a.ScrollToFixed.defaultOptions={marginTop:0,limit:0,bottom:-1,zIndex:1000,baseClassName:"scroll-to-fixed-fixed"};a.fn.scrollToFixed=function(b){return this.each(function(){(new a.ScrollToFixed(this,b))})}})(jQuery);
;
jQuery(function () {
  myPage.init();
});

var myPage = (function ($) {
  var that = {};

  that.init = function () {
    $('#header').scrollToFixed();
  }

  return that;
})(jQuery);
;
