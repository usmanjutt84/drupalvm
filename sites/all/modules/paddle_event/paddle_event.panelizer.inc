<?php
/**
 * @file
 * paddle_event.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function paddle_event_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'events';
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->name = 'node:events:default';
  $panelizer->css_id = '';
  $panelizer->css_class = '';
  $panelizer->css = '';
  $panelizer->no_blocks = FALSE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'paddle_content_region';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'paddle_2_col_3_9';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'right' => NULL,
      'left' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'e1362b45-8a4d-4373-bf4f-3a5908a1bf72';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'node:events:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-174ecb1c-bc54-4820-9ee9-0247e39d193f';
  $pane->panel = 'left';
  $pane->type = 'entity_revision_view';
  $pane->subtype = 'node';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '174ecb1c-bc54-4820-9ee9-0247e39d193f';
  $display->content['new-174ecb1c-bc54-4820-9ee9-0247e39d193f'] = $pane;
  $display->panels['left'][0] = 'new-174ecb1c-bc54-4820-9ee9-0247e39d193f';
  $pane = new stdClass();
  $pane->pid = 'new-993e3539-bfae-47fd-a944-2934caab0ea0';
  $pane->panel = 'right';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_show_breadcrumb';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '993e3539-bfae-47fd-a944-2934caab0ea0';
  $display->content['new-993e3539-bfae-47fd-a944-2934caab0ea0'] = $pane;
  $display->panels['right'][0] = 'new-993e3539-bfae-47fd-a944-2934caab0ea0';
  $pane = new stdClass();
  $pane->pid = 'new-ffd384c8-d5a8-492e-9401-94bc29581d86';
  $pane->panel = 'right';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'ffd384c8-d5a8-492e-9401-94bc29581d86';
  $display->content['new-ffd384c8-d5a8-492e-9401-94bc29581d86'] = $pane;
  $display->panels['right'][1] = 'new-ffd384c8-d5a8-492e-9401-94bc29581d86';
  $pane = new stdClass();
  $pane->pid = 'new-45e78f6d-11b0-4a88-b5a2-c0c9aab4a08c';
  $pane->panel = 'right';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_paddle_agenda';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '45e78f6d-11b0-4a88-b5a2-c0c9aab4a08c';
  $display->content['new-45e78f6d-11b0-4a88-b5a2-c0c9aab4a08c'] = $pane;
  $display->panels['right'][2] = 'new-45e78f6d-11b0-4a88-b5a2-c0c9aab4a08c';
  $pane = new stdClass();
  $pane->pid = 'new-d2d4a183-c198-48e2-bc43-d5398073f7ab';
  $pane->panel = 'right';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_paddle_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'd2d4a183-c198-48e2-bc43-d5398073f7ab';
  $display->content['new-d2d4a183-c198-48e2-bc43-d5398073f7ab'] = $pane;
  $display->panels['right'][3] = 'new-d2d4a183-c198-48e2-bc43-d5398073f7ab';
  $pane = new stdClass();
  $pane->pid = 'new-a38abdea-5292-41d6-af4a-63a74d80a205';
  $pane->panel = 'right';
  $pane->type = 'content_region';
  $pane->subtype = 'content_region';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'region' => 'bottom',
    'type' => 'all_pages',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = 'a38abdea-5292-41d6-af4a-63a74d80a205';
  $display->content['new-a38abdea-5292-41d6-af4a-63a74d80a205'] = $pane;
  $display->panels['right'][4] = 'new-a38abdea-5292-41d6-af4a-63a74d80a205';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-ffd384c8-d5a8-492e-9401-94bc29581d86';
  $panelizer->display = $display;
  $export['node:events:default'] = $panelizer;

  return $export;
}
